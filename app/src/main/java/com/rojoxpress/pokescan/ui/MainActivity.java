package com.rojoxpress.pokescan.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.rojoxpress.pokescan.R;
import com.rojoxpress.pokescan.utils.ShPreferences;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main2);

        RequestQueue queue = Volley.newRequestQueue(this);

        ShPreferences preferences = new ShPreferences(this);

        if(!preferences.hasResetDistance()){
            preferences.setResetDistance(true);
            preferences.setDistance(3);
        }

        Intent intent = new Intent(this,LoginActivity.class);
        if(preferences.hasLogin()){
            intent = new Intent(this,MapScannerActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
