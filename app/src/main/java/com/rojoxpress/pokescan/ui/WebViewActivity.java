package com.rojoxpress.pokescan.ui;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.rojoxpress.pokescan.R;
import com.rojoxpress.pokescan.databinding.ActivityWebViewBinding;
import com.rojoxpress.pokescan.utils.Utils;

import java.util.Locale;

public class WebViewActivity extends BaseActivity {

    ActivityWebViewBinding binding;
    public static final String FILE = "file";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String file = getIntent().getStringExtra("file");
        Locale current = getResources().getConfiguration().locale;

        if(file.equals("info")){
            binding.shadow.setVisibility(View.VISIBLE);
            binding.buttonBar.setVisibility(View.VISIBLE);
        }

        if(current.getLanguage().equals("es")){
            file = file + "_" + current.getLanguage();
        }
        binding.webView.loadUrl("file:///android_asset/"+file+".html");


        binding.webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Utils.openInChromeTabs(WebViewActivity.this, Uri.parse(url));
                return true;
            }
        });
    }

    public void onClick(View view) {
        String message = "\uD83D\uDC4D";
        if(view.getId() == R.id.bad_button){
            setResult(LoginActivity.LOCAL_LOGIN);
            message = "\uD83D\uDE22";
        }
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
