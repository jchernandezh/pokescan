package com.rojoxpress.pokescan.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.rojoxpress.pokescan.R;
import com.rojoxpress.pokescan.databinding.ActivityLoginBinding;
import com.rojoxpress.pokescan.http.LoginHandler;
import com.rojoxpress.pokescan.utils.ShPreferences;
import com.rojoxpress.pokescan.utils.Utils;

import java.util.Random;

public class LoginActivity extends BaseActivity {

    private ActivityLoginBinding binding;
    private ProgressDialog progressDialog;
    private LoginHandler loginHandler;
    private ShPreferences preferences;
    public static final int LOCAL_LOGIN = 500;
    private boolean annonUser = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        preferences = new ShPreferences(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(loginHandler != null){
            loginHandler.cancel(true);
        }
    }

    public void login(final String username, final String password){

      //  Log.d("data",username+","+password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.login_in));
        progressDialog.setCancelable(false);
        progressDialog.show();
        loginHandler = new LoginHandler();
        loginHandler.execute(username,password);
        loginHandler.setCallback(new LoginHandler.LoginCallback() {
            @Override
            public void onLogin(String token) {
                Log.i("token",token);
                progressDialog.dismiss();
                preferences.setPoken(token);
                preferences.setLogin(true);
                preferences.setUsername(username);
                preferences.setPassword(password);
                preferences.setAnnonUser(annonUser);
                startActivity(new Intent(LoginActivity.this,MapScannerActivity.class));
                finish();
            }

            @Override
            public void onError() {
                Log.i("token","error");
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage(getString(R.string.login_message,"\uD83D\uDE09"));
                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == DialogInterface.BUTTON_POSITIVE){
                            if(annonUser){
                                localLogin();
                            } else {
                                findViewById(R.id.login_button).performClick();
                            }
                        } else {
                            Utils.openInChromeTabs(LoginActivity.this,Uri.parse("http://ispokemongodownornot.com"));
                        }
                    }
                };
                builder.setPositiveButton(R.string.retry_login,listener);
                builder.setNegativeButton(R.string.check_poke_servers,listener);
                builder.show();
                progressDialog.dismiss();
                annonUser = false;
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button:{
                if(validateFields())
                    login(binding.usernameEt.getText().toString(),binding.passwordEt.getText().toString());
            }break;
            case R.id.fake_button:{
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.sign_up_explain);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Utils.openInChromeTabs(LoginActivity.this,Uri.parse(getString(R.string.sign_in_url)));
                    }
                });
                builder.show();
            }break;
            case R.id.skip:{
                Intent intent = new Intent(this,WebViewActivity.class);
                intent.putExtra(WebViewActivity.FILE,"info");
                startActivityForResult(intent,LOCAL_LOGIN);
            }break;
            case R.id.about_button:{
                Intent intent = new Intent(this,WebViewActivity.class);
                intent.putExtra(WebViewActivity.FILE,"about");
                startActivity(intent);
            }break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LOCAL_LOGIN && resultCode == LOCAL_LOGIN){
            annonUser = true;
            localLogin();
        } else {
            Utils.openInChromeTabs(LoginActivity.this,Uri.parse(getString(R.string.sign_in_url)));
            annonUser = false;
        }
    }

    public void localLogin(){
        String username = "redapps";
        String password = "rojoazul";
        Random r = new Random();
        int Low = 0;
        int High = 12;
        int result = r.nextInt(High-Low) + Low;
        if(result > 1){
            username = username+result;
        }
       login(username,password);
    }

    public boolean validateFields(){
        if(binding.usernameEt.getText().length() == 0){
            binding.usernameIn.setError("Invalid field");
            return false;
        }
        if(binding.passwordEt.getText().length() == 0){
            binding.passwordIn.setError("Invalid field");
            return false;
        }
        return true;
    }
}
