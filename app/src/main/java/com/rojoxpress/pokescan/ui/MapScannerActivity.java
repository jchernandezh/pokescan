package com.rojoxpress.pokescan.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pokegoapi.api.PokemonGo;
import com.pokegoapi.api.map.Map;
import com.pokegoapi.api.map.pokemon.CatchablePokemon;
import com.pokegoapi.auth.PtcCredentialProvider;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.RemoteServerException;
import com.rojoxpress.pokescan.R;
import com.rojoxpress.pokescan.http.LoginHandler;
import com.rojoxpress.pokescan.utils.BitmapLruCache;
import com.rojoxpress.pokescan.utils.ShPreferences;
import com.rojoxpress.pokescan.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;

public class MapScannerActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {


    private GoogleMap geoMap;
    ArrayList<LatLng> locations;
    public static final int ACCEPT_PERMISSION = 10;
    private Location uLocation;
    private PokemonGo pokemonGo;
    private GoogleApiClient mGoogleApiClient;
    private CatchablePokemonsTask pokeAsync;
    private ProgressDialog progressDialog;
    private FloatingActionButton fab;
    private ArrayMap<Marker,CatchablePokemon> markerArrayMap;
    private ImageLoader imageLoader;
    private CardView progressView;
    private ProgressBar progressBar;
    private TextView progressText;
    private MenuItem rangeItem;
    private PopupWindow popupWindow;
    private int MIN_DISTANCE = 2, SCALE_DISTANCE = 100;
    private ShPreferences preferences;
    private boolean isTextPopUpVisible = false;
    private LoginHandler loginHandler;
    private int errorCount;
    private ArrayList<Marker> debugMarkers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        fab = (FloatingActionButton) findViewById(R.id.fab_scan);

        locations = new ArrayList<>();
        markerArrayMap = new ArrayMap<>();

        loginHandler = new LoginHandler();

        preferences = new ShPreferences(this);


        progressText = (TextView) findViewById(R.id.progress_text);
        progressView = (CardView) findViewById(R.id.progress_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        progressBar.setMax(100);


        imageLoader = new ImageLoader(Volley.newRequestQueue(getApplicationContext()),new BitmapLruCache());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Login in");
        progressDialog.setCancelable(false);




        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        boolean needsPermission = true;
        for (String permission : permissions) {
            needsPermission = needsPermission && ActivityCompat.checkSelfPermission(this,permission)!= PackageManager.PERMISSION_GRANTED;
        }
        if (needsPermission) {
            ActivityCompat.requestPermissions(this,permissions, ACCEPT_PERMISSION);
        } else {
            SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map,menu);
        rangeItem = menu.findItem(R.id.range_distance);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.range_distance:{
                if(popupWindow!= null){
                    if(popupWindow.isShowing()){
                        popupWindow.dismiss();
                        findViewById(R.id.touch_out).setVisibility(View.GONE);
                        return super.onOptionsItemSelected(item);
                    }
                }
                distancePopUp();
                if(!preferences.hasDisclaimerShown()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.caution);
                    builder.setMessage(R.string.data_disclaimer);
                    builder.setPositiveButton("OK", null);
                    builder.show();
                    preferences.setDisclaimerShow(true);
                }
            }break;
            case R.id.about:{
                Intent intent = new Intent(this,WebViewActivity.class);
                intent.putExtra(WebViewActivity.FILE,"about");
                startActivity(intent);
            }break;
            case R.id.logout:{
               logout();
            }break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void logout(){
        preferences.deletePrefs();
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }

    synchronized void buildClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ACCEPT_PERMISSION) {

            boolean grantPermissions = true;

            for(int grant : grantResults){
                grantPermissions = grant == PackageManager.PERMISSION_GRANTED;
            }

            if(grantPermissions) {
                SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
                mapFragment.getMapAsync(this);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        geoMap = googleMap;
        geoMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.setOnMarkerClickListener(this);
        buildClient();

        if(!preferences.hasDialogShown()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.i_got_it, null);
            builder.setTitle(R.string.caution);
            builder.setMessage(R.string.caution_new_api);
            builder.show();
            preferences.setDialogShown(true);
        }

    }

    public void distancePopUp() {

        View view = View.inflate(this, R.layout.popup_resize_text, null);
        final TextView distanceText = (TextView) view.findViewById(R.id.range_distance);
        int distance = preferences.getDistance() * SCALE_DISTANCE;
        distanceText.setText(String.format(Locale.getDefault(),"Aprox: %dm",distance));
        float density = getResources().getDisplayMetrics().density;
        int dpHeight = (int) (110 * density);
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, dpHeight);
        popupWindow.setContentView(view);
        popupWindow.showAsDropDown(view, 0, findViewById(R.id.toolbar).getHeight() + (findViewById(R.id.toolbar).getHeight() / 2));
        final SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBarText);
        seekBar.setProgress(preferences.getDistance() - MIN_DISTANCE);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                preferences.setDistance(i + MIN_DISTANCE);
                int distance = preferences.getDistance()*SCALE_DISTANCE;
                distanceText.setText(String.format(Locale.getDefault(),"Aprox: %dm",distance));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        isTextPopUpVisible = true;
        findViewById(R.id.touch_out).setVisibility(View.VISIBLE);
    }



    public void setLocations(){
        double t = 0;
        int count = 0;
        Location lastLocation = new Location("");
        debugMarkers = new ArrayList<>();
        locations = new ArrayList<>();
        locations.add(new LatLng(uLocation.getLatitude(),uLocation.getLongitude()));
        lastLocation.setLatitude(uLocation.getLatitude());
        lastLocation.setLongitude(uLocation.getLongitude());
        do {

            double newLat = (uLocation.getLatitude() + (t * Math.cos(8000 * t)));
            double newLong = -(-uLocation.getLongitude() + (t * Math.sin(8000 * t)));
            LatLng latLng = new LatLng(newLat, newLong);
            Location temp = new Location("");
            temp.setLatitude(newLat);
            temp.setLongitude(newLong);

            if(lastLocation.distanceTo(temp)>=90){
//                geoMap.addMarker(new MarkerOptions().position(latLng));
                lastLocation.setLongitude(latLng.longitude);
                lastLocation.setLatitude(latLng.latitude);
                locations.add(latLng);
            }

            t = t + 0.00002;
            count ++;
        }while (lastLocation.distanceTo(uLocation)<preferences.getDistance()*SCALE_DISTANCE);
//        Log.i("locations",""+locations.size());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            if(geoMap != null){
                buildClient();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(loginHandler != null){
            loginHandler.cancel(true);
        }
        if(pokeAsync != null){
            pokeAsync.cancel(true);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //noinspection ResourceType
        Location newLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        //noinspection ResourceType
        geoMap.setMyLocationEnabled(true);
        if(newLocation != null){
            if(uLocation != null){
                if(uLocation.distanceTo(newLocation) < 500){
                    return;
                }
            }
            uLocation = newLocation;
            mGoogleApiClient.disconnect();
            LatLng target = new LatLng(uLocation.getLatitude(), uLocation.getLongitude());

            CameraPosition.Builder builder = new CameraPosition.Builder();
            builder.zoom(15);
            builder.target(target);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pokeAsync = new CatchablePokemonsTask();
                    pokeAsync.execute();
                }
            });

            geoMap.moveCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
            Snackbar.make(findViewById(R.id.coordinator),"",Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.scan, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pokeAsync = new CatchablePokemonsTask();
                            pokeAsync.execute();
                        }
                    })
                    .setCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar snackbar, int event) {
                            super.onDismissed(snackbar, event);
                            if(event != Snackbar.Callback.DISMISS_EVENT_ACTION){
                                fab.setVisibility(View.VISIBLE);
                            }
                        }
                    }).show();
        } else {
            Toast.makeText(this,"User location not found",Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }



    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if(markerArrayMap!= null && markerArrayMap.get(marker) != null) {
            CatchablePokemon pokemon = markerArrayMap.get(marker);
            long now = new Date().getTime();
            long timeLeft = pokemon.getExpirationTimestampMs() - now;
            String timeString = String.format(Locale.getDefault(),"%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(timeLeft) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeLeft)),
                    TimeUnit.MILLISECONDS.toSeconds(timeLeft) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeLeft)));
            long seconds = TimeUnit.MILLISECONDS.toSeconds(timeLeft);
            if (timeLeft >= 0) {
                marker.setSnippet("Time Left: " + timeString);
            } else {
                Toast.makeText(this, "Expired \uD83D\uDE22", Toast.LENGTH_SHORT).show();
                marker.remove();
            }
        }
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if(popupWindow != null && popupWindow.isShowing()){
            popupWindow.dismiss();
        }
        isTextPopUpVisible = false;
    }

    public void onClick(View view) {
        if(view.getId() ==  R.id.touch_out){
            findViewById(R.id.touch_out).setVisibility(View.GONE);
            if(popupWindow != null && popupWindow.isShowing()){
                popupWindow.dismiss();
            }
        }
    }

    public void login(String username, String password){

        progressDialog = new ProgressDialog(MapScannerActivity.this);
        progressDialog.setMessage(getString(R.string.login_in));
        progressDialog.setCancelable(false);
        progressDialog.show();
        loginHandler = new LoginHandler();
        loginHandler.execute(username,password);
        loginHandler.setCallback(new LoginHandler.LoginCallback() {
            @Override
            public void onLogin(String token) {
                progressDialog.dismiss();
                preferences.setPoken(token);
                preferences.setLogin(true);
                pokeAsync = new CatchablePokemonsTask();
                pokeAsync.execute();
            }

            @Override
            public void onError() {
//                Log.i("token","error");
                Toast.makeText(MapScannerActivity.this,"Login error",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                logout();

            }
        });
    }

    class CatchablePokemonsTask extends AsyncTask<Void, ArrayList<CatchablePokemon>, Boolean> {


        private boolean loginError = false;
        private String errorMessage;
        private OkHttpClient httpClient = new OkHttpClient();
        private long lastUpdate;

        @Override
        protected void onPostExecute(Boolean error) {
            super.onPostExecute(error);
            fab.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.GONE);
            rangeItem.setVisible(true);
            pokeAsync.cancel(true);

            if(!error) {
                if(markerArrayMap.size() ==0){
                    Toast.makeText(MapScannerActivity.this, R.string.not_pokemon_found, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MapScannerActivity.this, "Done", Toast.LENGTH_SHORT).show();
                }
                this.cancel(true);
            } else {
                if(loginError  && markerArrayMap.isEmpty()){
                    Toast.makeText(MapScannerActivity.this, String.format(Locale.getDefault(),
                            "%s , %s",getString(R.string.login_error),getString(R.string.retrying)), Toast.LENGTH_SHORT).show();
                    errorCount++;
                     pokemonGo = null;
                    String username;
                    final String password;
                    if(preferences.isAnnonUser()){
                        username = "redapps";
                        password = "rojoazul";
                        Random r = new Random();
                        int Low = 0;
                        int High = 12;
                        int result = r.nextInt(High-Low) + Low;
                        if(result != 0){
                            username = username+result;
                        }
                    } else {
                        username = preferences.getUsername();
                        password = preferences.getPassword();
                    }

                    if(errorCount >= 3){
                        AlertDialog.Builder builder = new AlertDialog.Builder(MapScannerActivity.this);
                        builder.setTitle(R.string.login_error);
                        builder.setMessage(getString(R.string.login_message,"\uD83D\uDE09"));
                        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == DialogInterface.BUTTON_POSITIVE)
                                   logout();
                                else
                                    Utils.openInChromeTabs(MapScannerActivity.this, Uri.parse("http://ispokemongodownornot.com"));
                            }
                        };
                        builder.setPositiveButton(R.string.logout,listener);
                        builder.setNegativeButton(R.string.check_poke_servers,listener);
                        builder.show();
                    } else {

                        login(username,password);
                    }

                } else {
                    if(markerArrayMap.size() > 0){
                        Toast.makeText(MapScannerActivity.this, R.string.pokemon_go_server_error, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(MapScannerActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(uLocation == null){
                Toast.makeText(MapScannerActivity.this,"User location not found",Toast.LENGTH_SHORT).show();
                cancel(true);
            }

            setLocations();

            isTextPopUpVisible = false;
            if(popupWindow != null && popupWindow.isShowing()){
                popupWindow.dismiss();
                findViewById(R.id.touch_out).setVisibility(View.GONE);
            }
//            Log.i("distance",""+locations.get(1).getDistance());
            rangeItem.setVisible(false);
            progressView.setVisibility(View.VISIBLE);
            progressText.setText(getString(R.string.searching,0));
            progressBar.setProgress(0);
            fab.setVisibility(View.GONE);
            geoMap.clear();
            markerArrayMap = new ArrayMap<>();
            lastUpdate = new Date().getTime();
        }

        private int count = 0;

        @Override
        protected Boolean doInBackground(Void... params) {

            System.out.println("background");
            boolean hasErrors = false;
            int counter = 0;
            do{
                ArrayList<CatchablePokemon> pokemons = new ArrayList<>();
                try {
                    if(pokemonGo == null){
                        pokemonGo = new PokemonGo(new PtcCredentialProvider(httpClient,preferences.getUsername(),preferences.getPassword()),httpClient);
                        lastUpdate = new Date().getTime();
                    }
                    long now = new Date().getTime();
                    long elapsed = now -lastUpdate;
                    if(elapsed < 3000){
                        continue;
                    }
                    pokemonGo.setLatitude(locations.get(counter).latitude);
                    pokemonGo.setLongitude(locations.get(counter).longitude);
                    pokemons.addAll(pokemonGo.getMap().getCatchablePokemon());
//                    Log.i("progress",""+counter);

                } catch (LoginFailedException | RemoteServerException e) {
                    if(e instanceof LoginFailedException){
                        loginError = true;
                    }
                    errorMessage = getString(R.string.server_error);
                    Log.w("progress",""+counter);
                    hasErrors = true;
                }
                lastUpdate = new Date().getTime();
                counter++;
                publishProgress(pokemons);
            } while (counter < locations.size());
            return hasErrors;
        }

        @Override
        protected void onProgressUpdate(ArrayList<CatchablePokemon> ... values) {
            super.onProgressUpdate(values);
            double percent = 100*((double)count/(double) locations.size());
            progressText.setText(getString(R.string.searching,(int)percent));
            progressBar.setProgress((int) percent);
            ArrayList<CatchablePokemon> pokemons = new ArrayList<>();

            pokemons.addAll(values[0]);

            if(!pokemons.isEmpty()) {
                for (int i = 0; i < pokemons.size(); i++) {
                    CatchablePokemon pokemon = pokemons.get(i);
                    if (isPokemonOnMap(pokemon)) {
                        continue;
                    }
                    final Marker marker = geoMap.addMarker(new MarkerOptions()
                            .title(pokemon.getPokemonId().name())
                            .position(new LatLng(pokemon.getLatitude(), pokemon.getLongitude())).anchor(1, 1));
                    markerArrayMap.put(marker, pokemon);
                    imageLoader.get("http://pokeapi.co/media/sprites/pokemon/" +
                            pokemon.getPokemonId().getNumber() + ".png", new ImageLoader.ImageListener() {
                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                            if (response.getBitmap() != null) {
                                int size = Utils.dpToPixels(60, MapScannerActivity.this);
                                Bitmap bitmap = Bitmap.createScaledBitmap(response.getBitmap(), size, size, false);
                                marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
                                marker.setAnchor(0.5f, 0.5f);
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                }
            }
            count++;
        }
    }


    public boolean isPokemonOnMap(CatchablePokemon pokemon){
        for(int i = 0;i<markerArrayMap.size();i++){
            CatchablePokemon mapPoke = markerArrayMap.valueAt(i);
            if(pokemon.getPokemonId().equals(pokemon.getPokemonId())){
                if(pokemon.getLatitude() ==  mapPoke.getLatitude() && mapPoke.getLongitude() == pokemon.getLongitude())
                    return true;
            }
        }
        return false;
    }


}
