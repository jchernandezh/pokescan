package com.rojoxpress.pokescan.ui;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.rojoxpress.pokescan.R;

public class PermissionRequestActivity extends BaseActivity {

    public static final int ACCEPT_PERMISSION = 10;
    public static final String PERMISSONS = "permissions";
    private String[] permissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission_request);

        /*DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);


        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = Utils.dpToPixels(200,this);
        params.width = metrics.widthPixels - Utils.dpToPixels(20,this);
        params.gravity = Gravity.CENTER;

        this.getWindow().setAttributes(params);*/

        permissions = getIntent().getStringArrayExtra(PERMISSONS);


    }

    @Override
    protected void onResume() {
        super.onResume();

        if(permissions != null){
           /* boolean needsPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
            needsPermission = needsPermission && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
*/
            boolean needsPermission = true;
            for (String permission : permissions) {
                needsPermission = needsPermission && ActivityCompat.checkSelfPermission(this,permission)!= PackageManager.PERMISSION_GRANTED;
            }
            if (needsPermission) {
                ActivityCompat.requestPermissions(this,permissions, ACCEPT_PERMISSION);
            } else {
                setResult(ACCEPT_PERMISSION);
                finish();
            }
        } else {
            finish();
        }


    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ACCEPT_PERMISSION) {

            boolean grantPermissions = true;

            for(int grant : grantResults){
                grantPermissions = grant == PackageManager.PERMISSION_GRANTED;
            }

            if(grantPermissions) {
                setResult(ACCEPT_PERMISSION);
            }
        }
        finish();
    }
}
