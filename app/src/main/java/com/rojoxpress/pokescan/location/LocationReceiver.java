package com.rojoxpress.pokescan.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rojoxpress.pokescan.mApplication;
import com.rojoxpress.pokescan.utils.ShPreferences;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LocationReceiver extends BroadcastReceiver {

    ShPreferences configurations;
    mApplication application;

    @Override
    public void onReceive(final Context context, Intent intent) {

        final Location location = (Location) intent.getExtras().get("com.google.android.location.LOCATION");

        configurations = new ShPreferences(context);
        application = (mApplication) context.getApplicationContext();
        if(location!=null && configurations.hasLogin()){
            Log.i("Update","Check update");
            if(doLocationUpdate(location)){


                /*if(application.mQueue==null) {
                    application.mQueue = Volley.newRequestQueue(context);
                }*/
                Log.i("Update","Do update");
               /* mRequests mRequest = new mRequests(context,application.mQueue) {
                    @Override
                    public void callBackResponse(boolean error, boolean responseOK, String message, Object... args) {
                        if(!error){
                            if(responseOK){
                                configurations.setLastLocation(location.getLatitude()+","+location.getLongitude());
                                configurations.setLastLocationTime(new Date().getTime());
                            }
                        }else {
                            if(args[0]!=null) {
                                int code = ((int) args[0]);
                                if (code == 401) {
                                    final Configurations config = new Configurations(context);
                                    config.increaseCount401();
                                    long countTime = config.getTimeCount401();
                                    long now = new Date().getTime();
                                    long diff = (now - countTime);
                                    long days = TimeUnit.DAYS.toMillis(1);
                                    if (diff >= days) {
                                        config.resetCount401();
                                        config.setTimeCount401(new Date().getTime());
                                    }
                                    if (config.getCount401() == 10) {
                                        Log.e("error","geos error");
                                        String url = context.getString(R.string.urlSafe)+"/geos/error";
                                        StringRequest addError = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String s) {
                                            }
                                        }, new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                            }
                                        }){
                                            @Override
                                            protected Map<String, String> getParams() throws AuthFailureError {

                                                Map<String,String> params = new HashMap<>();
                                                params.put("urban_id",config.getUrbanID());

                                                return params;
                                            }
                                        };
                                        application.mQueue.add(addError);
                                    }
                                }
                            }
                        }
                    }};
                mRequest.updateLocation(location);*/
            }
        }
    }


    public boolean doLocationUpdate(Location newLocation){

        //DISTANCE IN METERS , TIME IN MINUTES
        int MIN_DISTANCE = 500,MIN_TIME = 60;
        return true;
        /*if(configurations.getLastLocation()!=null){
            String[] configLocation = configurations.getLastLocation().split(",");
            Location lasLocation = new Location("");
            lasLocation.setLatitude(Double.parseDouble(configLocation[0]));
            lasLocation.setLongitude(Double.parseDouble(configLocation[1]));
            float distance = lasLocation.distanceTo(newLocation);
            if(distance>MIN_DISTANCE){
                return true;
            }else{
                long time =configurations.getRefreshTime();
                long now = new Date().getTime();
                long diff =now-time;
                long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

                return minutes >= MIN_TIME;
            }
        }else {
            return true;
        }*/
    }
}