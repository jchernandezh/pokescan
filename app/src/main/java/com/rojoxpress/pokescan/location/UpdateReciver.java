package com.rojoxpress.pokescan.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.rojoxpress.pokescan.utils.ShPreferences;

public class UpdateReciver extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent) {

        if(new ShPreferences(context).hasLogin()) {
            context.startService(new Intent(context, UpdateService.class));
        }

    }
}
