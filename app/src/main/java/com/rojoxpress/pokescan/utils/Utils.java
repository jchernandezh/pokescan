package com.rojoxpress.pokescan.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;

import com.rojoxpress.pokescan.utils.CustomTabActivityHelper;

/**
 * Created by rojo on 22/07/16.
 */
public class Utils {
    /**
     * Method to convert Android dp to pixels depending on screen resolution
     * @param dp int of dp to convert
     * @param context Application Context
     * @return int converted pixels size
     */

    public static int dpToPixels(int dp, Context context) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    public static void  openInChromeTabs(Activity activity, Uri url){

        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
        CustomTabActivityHelper.openCustomTab(activity, customTabsIntent, url,
                new CustomTabActivityHelper.CustomTabFallback() {
                    @Override
                    public void openUri(Activity activity, Uri uri) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                        activity.startActivity(browserIntent);
                    }
                });

    }
}
