package com.rojoxpress.pokescan.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rojo on 23/07/16.
 */
public class ShPreferences {

    private String FILE_NAME = "preferences";
    private String DISTANCE = "location_distance";
    private String USERNAME = "username";
    private String PASSWORD = "password";
    private String POKEN    = "poken";
    private String LOGIN = "login";
    private String DATA_DISC = "data_disc";
    private String ANNON_USER = "annon_user";
    private String CAUTION_DIALOG = "caution_dialog";

    private String REST_DISTANCE = "reset_distance";

    private Context context;


    public ShPreferences(Context context){
        this.context = context;
    }

    public void deletePrefs(){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.clear();
        editor.apply();

    }

    private SharedPreferences getPreferences(){
        return context.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
    }

    public void setDistance(int value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(DISTANCE,value);
        editor.apply();
    }

    public int getDistance(){
        return getPreferences().getInt(DISTANCE,6);
    }

    public void setUsername(String value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(USERNAME,value);
        editor.apply();
    }


    public String getPassword(){
        return getPreferences().getString(PASSWORD,null);
    }
    public void setPassword(String value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(PASSWORD,value);
        editor.apply();
    }


    public String getPoken(){
        return getPreferences().getString(POKEN,null);
    }
    public void setPoken(String value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(POKEN,value);
        editor.apply();
    }


    public String getUsername(){
        return getPreferences().getString(USERNAME,null);
    }

    public boolean hasLogin(){
        return getPreferences().getBoolean(LOGIN,false);
    }

    public void setLogin(boolean value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putBoolean(LOGIN,value);
        editor.apply();
    }

    public boolean hasDisclaimerShown(){
        return getPreferences().getBoolean(DATA_DISC,false);
    }

    public void setDisclaimerShow(boolean value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putBoolean(DATA_DISC,value);
        editor.apply();
    }
    public boolean isAnnonUser(){
        return getPreferences().getBoolean(ANNON_USER,false);
    }

    public void setAnnonUser(boolean value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putBoolean(ANNON_USER,value);
        editor.apply();
    }

    public boolean hasResetDistance(){
        return getPreferences().getBoolean(REST_DISTANCE,false);
    }

    public void setResetDistance(boolean value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putBoolean(REST_DISTANCE,value);
        editor.apply();
    }


    public boolean hasDialogShown(){
        return getPreferences().getBoolean(CAUTION_DIALOG,false);
    }

    public void setDialogShown(boolean value){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putBoolean(CAUTION_DIALOG,value);
        editor.apply();
    }


}
