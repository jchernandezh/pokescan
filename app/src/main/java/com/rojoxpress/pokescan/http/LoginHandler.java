package com.rojoxpress.pokescan.http;

import android.os.AsyncTask;
import android.util.Log;

import com.pokegoapi.auth.PtcCredentialProvider;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.RemoteServerException;

import POGOProtos.Networking.Envelopes.RequestEnvelopeOuterClass;
import okhttp3.OkHttpClient;

public class LoginHandler extends AsyncTask<String,Void,String> {
    public LoginHandler(){
    }
    OkHttpClient httpClient = new OkHttpClient();
    private LoginCallback callback;

    @Override
    protected void onPostExecute(String token) {
        super.onPostExecute(token);
        if(token != null)
            callback.onLogin(token);
        else {
            callback.onError();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i("Pokemon","login");
            PtcCredentialProvider provider = new PtcCredentialProvider(httpClient,params[0],params[1]);
            return provider.getTokenId();

        } catch (LoginFailedException | RemoteServerException e) {
            e.printStackTrace();
            Log.d("failed",params[0]+":"+params[1]);
        }

        return null;
    }

    public void setCallback(LoginCallback callback) {
        this.callback = callback;
    }

    public interface LoginCallback {
        void onLogin(String token);
        void onError();
    }
}
